# Embulk Docker image

This image build the embulk utility in a convenient docker image.

## Documentation

Embulk is a declarative data moving utility.
It takes a YAML-file declaration, that declares an input and an output to move
data between a source and a target.

Sources and targets can be _plugins_, which make Embulk extensible.
In this Docker image, we use `embulk-input-postgresql` and `embulk-output-postgresql`.

It comes with a couple of built-in capabilities, such as `csv` or `stdout`.

The follwing is an example YAML task declaration, that runs a query on a Postgres
database, and streams the results to `stdout`.

```
.postgresql: &postgresql
  type: postgresql
  host: '...'
  user: '...'
  password: '...'
  database: '...'
  ssl: true
  # extend socket timeout for long running queries (seconds)
  socket_timeout: 10800
  options: {loglevel: 3}

in:
  <<: *postgresql
  query: |
    SELECT * FROM artists

out:
  type: stdout
```

You can run a task declaration using the following command:

```console
$ embulk run task.yml
```


### Templates

YAML task declarations support the liquid templating language.
Embulk reads environment variables, and lets you substitute its values in a task template.

Usually the task templates have the `.liquid` suffix on the file.

For example:

```
.postgresql: &postgresql
  type: postgresql
  host: '{{env.PGHOST}}'
  user: '{{env.PGUSER}}'
  password: '{{env.PGPASSWORD}}'
  database: '{{env.PGDATABASE}}'
  ssl: true
  # extend socket timeout for long running queries (seconds)
  socket_timeout: 10800
  options: {loglevel: 3}

in:
  <<: *postgresql
  query: |
    SELECT * FROM artists

out:
  type: stdout
```

You can now provide environment variables to be substitued in a templated YAML
task declaration:

```console

$ PGHOST=... PGUSER=... PGDATABASE=... PGPASSWORD=... embulk run task.yml.liquid
```

### Read more on the plugins

* embulk-output-postgresql https://github.com/embulk/embulk-output-jdbc/tree/master/embulk-output-postgresql
* embulk-input-postgresql https://github.com/embulk/embulk-input-jdbc/tree/master/embulk-input-postgresql

## Build

```
$ docker build . -t embulk
```

